MPCDF-specific OSC plugins
==========================

Installation
------------

Put the `.py` files into `~/.osc-plugins` to make them available for the osc command,
for example by

```
$> git clone https://gitlab.mpcdf.mpg.de/mpcdf/osc-plugins ~/.osc-plugins
```

Background
----------

* http://loh.pages.mpcdf.de/build-server/manual.pdf
* https://en.opensuse.org/openSUSE:OSC_plugins

