#!/usr/bin/python2
from __future__ import print_function

import mpcdf_common

import os
import osc
import osc.conf
import osc.core
import osc.cmdln


@osc.cmdln.option('--recreate', action="store_true",
                  help="Re-create the set of enabled repositories from the stored attributes on the server")
@osc.cmdln.option('--compiler-modules', nargs=1,
                  help="Restrict the set of compilers to use")
@osc.cmdln.option('--mpi-modules', nargs=1,
                  help="Restrict the set of MPI implementations to use")
@osc.cmdln.option('--cuda-modules', nargs=1,
                  help="Restrict the set of CUDA implementations to use")
@osc.cmdln.option('--pgi-modules', nargs=1,
                  help="Restrict the set of PGI compilers to use")
@osc.cmdln.option('--set', nargs=1, metavar="FLAGS",
                  help="Modify the set of enabled repositories, without this the current setup is displayed. "
                       "FLAGS is a comma-separated list of a subset of 'system', 'compilers', 'mpi', 'cuda', 'cuda_mpi', 'pgi', 'pgi_mpi'")
@osc.cmdln.option('--disable', action="store_true", metavar="FLAGS",
                  help="Disable building this package")
@osc.cmdln.alias("mpcdf_enable_repos")
def do_mpcdf_enable_repositories(self, subcmd, opts, *args):
    """${cmd_name}: Select all appropriate repositories for an MPCDF package

    Due to the large number of repository combinations at MPCDF it is
    cumbersome to enable all the appropriate repositories for a given package
    by hand.

    This command allows you to set the <enable/> flags for certain kinds of
    repositories at once.

    Without --set this command only displays the current configuration.

    It is possible to combine this with user-defined <disabled/> flags that
    override the settings of this command, for example to disable the build of
    the package with a certain repository from a given set (e.g. one
    troublesome compiler)

    Usage:
        osc ${cmd_name}                       [[PROJECT] PACKAGE]
        osc ${cmd_name} --set FLAGS [options] [[PROJECT] PACKAGE]
        osc ${cmd_name} --recreate            [[PROJECT] PACKAGE]
        osc ${cmd_name} --disable             [[PROJECT] PACKAGE]

    ${cmd_option_list}
    """

    if len(args) == 0:
        if osc.core.is_package_dir(os.curdir):
            package = osc.core.store_read_package(os.curdir)
            project = osc.core.store_read_project(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify PACKAGE or run command in an osc package checkout directory')

    elif len(args) == 1:
        package, = args
        project = osc.core.store_read_project(os.curdir)

    elif len(args) == 2:
        project, package = args
    else:
        raise osc.oscerr.WrongArgs("Too many arguments")

    api_url = self.get_api_url()

    if opts.disable:
        mpcdf_common.set_attribute_values(api_url, project, package, "MPCDF:enable_repositories", "")
    elif opts.set:
        mpcdf_common.set_attribute_values(api_url, project, package, "MPCDF:enable_repositories", opts.set.split(","))

        def set_or_remove(flag, attribute_name):
            if flag:
                print("Setting attribute", attribute_name, "to", flag)
                mpcdf_common.set_attribute_values(api_url, project, package, attribute_name, flag.split(","))
            elif mpcdf_common.has_attribute(api_url, project, package, attribute_name):
                print("Removing attribute", attribute_name, "from package")
                mpcdf_common.remove_attribute(api_url, project, package, attribute_name)

        set_or_remove(opts.compiler_modules, "MPCDF:compiler_modules")
        set_or_remove(opts.mpi_modules, "MPCDF:mpi_modules")
        set_or_remove(opts.cuda_modules, "MPCDF:cuda_modules")
        set_or_remove(opts.pgi_modules, "MPCDF:pgi_modules")

    if opts.recreate or opts.set or opts.disable:
        mpcdf_common.mpcdf_enable_repositories(api_url, project, package, verbose=True)
        if project == "software":
            mpcdf_common.sync_projects(api_url, package)

    elif (opts.compiler_modules or opts.mpi_modules or opts.cuda_modules):
        print("ERROR: Invalid arguments, try --help")

    else:
        try:
            repos = mpcdf_common.get_attribute_values(api_url, project, package, "MPCDF:enable_repositories")
        except Exception:
            repos = ()
            print("ERRROR: No attribute MPCDF:enable_repositories present, package unmanaged")
        if repos:
            def print_attr(description, attribute_name):
                try:
                    values = mpcdf_common.get_attribute_values(api_url, project, package, attribute_name)
                except Exception:
                    return
                print(description, ", ".join(values))

            print("Enabled for:", *repos)
            print_attr("- Subset of compiler modules set to:", "MPCDF:compiler_modules")
            print_attr("- Subset of MPI modules set to:", "MPCDF:mpi_modules")
            print_attr("- Subset of CUDA modules set to:", "MPCDF:cuda_modules")
            print_attr("- Subset of PGI modules set to:", "MPCDF:pgi_modules")
        else:
            print("Disabled")
